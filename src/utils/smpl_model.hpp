#pragma once

#include "Eigen.h"


template <typename T> using v2 = Eigen::Matrix<T, 2, 1>;
template <typename T> using v3 = Eigen::Matrix<T, 3, 1>;
template <typename T> using m3 = Eigen::Matrix<T, 3, 3>;
template <typename T> using m4 = Eigen::Matrix<T, 4, 4>;
template <typename T> using mX = Eigen::Matrix<T, Dynamic, Dynamic>;

template<typename T>
m3<T> rodrigues(const v3<T> &rvec)
{
	// Convert an axis-angle rotation vector to a rotation matrix
	T len = rvec.norm();
	if ((len) == static_cast<T>(0))
		return m3<T>::Identity();
	return Eigen::AngleAxis<T>(len, rvec / len).toRotationMatrix();
}

class SMPLModel {
public:
	// Imports a model which was converted to json
	SMPLModel(const std::string &json_path);

	// This function returns joint positions for the given pose and shape
	// values
	// pose contains 24 vectors (axis angle rotations),
	// shape contains 10 floats and
	// the return value contains 24 vectors (joint positions)
	template<typename T>
	std::vector<v3<T>> calculate_joint_positions(
		std::vector<v3<T>> pose, std::vector<T> shape) const
	{
		assert(pose.size() == 24);
		assert(shape.size() == 10);

		// Code based on SMPL code and
		// https://khanhha.github.io/posts/SMPL-model-introduction/

		// Apply the shape on joint locations in the rest pose
		// joints_shaped is called "J_onbetas" in SMPLify
		mX<T> joints_shaped =  m_joints_rest.cast<T>();
		for (int b = 0; b < 10; ++b) {
			//joints_shaped += T(m_j_for_betas[b]) * shape[b];
		}

		// Calculate transformation matrices for the given pose
		std::vector<m4<T>> transforms(24);
		// The first joint is the root joint.
		transforms[0].setIdentity();
		transforms[0].template block<3, 3>(0, 0) = rodrigues<T>(pose[0]);
		transforms[0].template block<3, 1>(0, 3) = joints_shaped.row(0).transpose();
		transforms[0].template block<1, 4>(3, 0) << T(0), T(0), T(0), T(1);

		// FIXME: this code can be optimized since we are only interested in the
		// transformed joint locations and not the whole transformation matrices
		for (int j = 1; j < 24; ++j) {
			m4<T> transform_local;
			v3<T> pos = joints_shaped.row(j).transpose();
			v3<T> pos_parent = joints_shaped.row(m_parent[j]).transpose();
			transform_local.template block<3, 3>(0, 0) = rodrigues<T>(pose[j]);
			transform_local.template block<3, 1>(0, 3) = pos - pos_parent;
			transform_local.template block<1, 4>(3, 0) << T(0), T(0), T(0), T(1);
			transforms[j] = transforms[m_parent[j]] * transform_local;
		}
		// transforms corresponds to "results_global" in SMPL

		// The translation vectors in the transformations are the joint positions
		// after applying the shape and pose
		// joints_shaped_posed is called "Jtr" in SMPLify
		std::vector<v3<T>> joints_shaped_posed(24);
		for (int j = 0; j < 24; ++j) {
			joints_shaped_posed[j] =
				transforms[j].template block<3, 1>(0, 3);
		}


		return joints_shaped_posed;
	};

	// Calculates an error value for unrealistic poses such as backwards bending
	// knees
	// called E_a in SMPLify
	template<typename T>
	T unrealistic_pose_error(std::vector<v3<T>> pose) const
	{
		// Based on SMPLify's code
		std::vector<T> jointvalues = {
			pose[18](1),  // 55
			-pose[19](1),  // 58
			-pose[4](0),  // 12
			-pose[5](0),  // 15
		};
		T err(0);
		for (const T &v : jointvalues) {
			err += exp(v);
		}
		return err;
	};

	// Calculates error values for a pose which does not correspond to the
	// mean pose
	// This error is weighted very weakly.
	// It can be used when there are not enough constraints for all 24 pose
	// values
	template<typename T>
	void nondefault_pose_error(std::vector<v3<T>> pose, T *residual) const
	{
		for (int k = 0; k < 24; ++k) {
			residual[k] = pose[k].norm() * T(0.01f);
		}
	};

	const Eigen::MatrixXf &get_joints_rest() const { return m_joints_rest; }

private:

	// The vertex locations when shape and pose values are all zero,
	// also called "vertex_template" or "v_template"
	// 6890x3
	Eigen::MatrixXf m_vertices_rest;

	// The joint locations when shape and pose values are all zero
	// 24x3
	Eigen::MatrixXf m_joints_rest;

	// 13776 vertex indices for the triangles
	std::vector<Eigen::Vector3i> m_face_indices;

	// The joint regressor maps vertex positions to joint positions
	// 24 x 6890
	Eigen::MatrixXf m_joint_regressor;

	// 2 vectors each of size 24
	std::vector<std::vector<int>> m_kinematic_tree;

	// m_parent maps from a joint index to the index of the parent joint
	// 24 entries, the first one is unused
	std::vector<int> m_parent;

	// Linear Blend Skinning weights, 6890 vectors each of size 24
	std::vector<std::vector<float>> m_weights;

	// 6890 x 3 x 10
	std::vector<std::vector<std::vector<float>>> m_shape_blend_shapes;

	// For each of the 10 shape parameters,
	// m_j_for_betas gives a small offset vector for each joint,
	// i.e. it corresponds to shape_blend_shapes for joints instead of vertices
	// 10 entries, each are 24x3 matrices
	std::vector<Eigen::MatrixXf> m_j_for_betas;

	// 6890 x 3 x 207
	std::vector<std::vector<std::vector<float>>> m_pose_blend_shapes;
};
