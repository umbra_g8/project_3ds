#include <iostream>
#include <fstream>
#include <assert.h>
#include "smpl_model.hpp"
#include "json.hpp"

using json = nlohmann::json;

// The number of vertices in the SMPL model
#define NUM_VERTICES 6890


SMPLModel::SMPLModel(const std::string &json_path):
	m_vertices_rest(NUM_VERTICES, 3),
	m_joint_regressor(24, NUM_VERTICES),
	m_parent(24, -1)
{
	std::cout << "Importing the model from " << json_path << std::endl;
	std::ifstream input_file(json_path);
	json j;
	input_file >> j;
	std::cout << "Model decoded" << std::endl;

	std::cout << "Extracting vertex_template" << std::endl;
	assert(j["vertices_template"].size() == NUM_VERTICES);
	for (int v = 0; v < NUM_VERTICES; ++v) {
		const json &vert = j["vertices_template"][v];
		m_vertices_rest.block<1, 3>(v, 0) =
			Eigen::Vector3f(vert[0], vert[1], vert[2]).transpose();
	}

	std::cout << "Extracting face_indices" << std::endl;
	for (const json &vert : j["face_indices"]) {
		m_face_indices.push_back(
			Eigen::Vector3i(vert[0], vert[1], vert[2]));
	}

	std::cout << "Extracting joint_regressor" << std::endl;
	assert(j["joint_regressor"].size() == 24);
	for (int ji = 0; ji < 24; ++ji) {
		const json &joint = j["joint_regressor"][ji];
		assert(joint.size() == NUM_VERTICES);
		for (int v = 0; v < NUM_VERTICES; ++v) {
			m_joint_regressor(ji, v) = joint[v];
		}
	}

	std::cout << "Extracting kinematic_tree" << std::endl;
	assert(j["kinematic_tree"].size() == 2);
	for (const json &tbl : j["kinematic_tree"]) {
		assert(tbl.size() == 24);
		m_kinematic_tree.push_back(tbl.get<std::vector<int>>());
	}

	std::cout << "Extracting weights" << std::endl;
	for (const json &w : j["weights"]) {
		m_weights.push_back(w.get<std::vector<float>>());
	}

	std::cout << "Extracting shape_blend_shapes" << std::endl;
	assert(j["shape_blend_shapes"].size() == NUM_VERTICES);
	m_shape_blend_shapes = j["shape_blend_shapes"].get<
		std::vector<std::vector<std::vector<float>>>>();

	// For some reason, nested vectors in the get method cannot be used
	// to extract some of the other fields.
	std::cout << "Extracting pose_blend_shapes" << std::endl;
	m_pose_blend_shapes = j["pose_blend_shapes"].get<
		std::vector<std::vector<std::vector<float>>>>();

	std::cout << "Precalculating joint locations etc." << std::endl;

	// Calculate joint locations in the rest pose with the regressor
	// and rest pose vertex locations
	m_joints_rest = m_joint_regressor * m_vertices_rest;

	for (int b = 0; b < 10; ++b) {
		Eigen::MatrixXf vertices_shape(NUM_VERTICES, 3);
		for (int v = 0; v < NUM_VERTICES; ++v) {
			for (int c = 0; c < 3; ++c) {
				vertices_shape(v, c) = m_shape_blend_shapes[v][c][b];
			}
		}
		m_j_for_betas.push_back(m_joint_regressor * vertices_shape);
	}

	// See global_rigid_transformation
	std::vector<int> id_to_col(24, -1);
	for (int j = 0; j < 24; ++j) {
		// FIXME: what is id_to_col good for?
		assert(m_kinematic_tree[1][j] == j);
		id_to_col[m_kinematic_tree[1][j]] = j;
	}
	// The root joint has no parent
	for (int j = 1; j < 24; ++j) {
		m_parent[j] = id_to_col[m_kinematic_tree[0][j]];
	}

	std::cout << "Done loading the model from " << json_path << std::endl;
}
