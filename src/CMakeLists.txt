cmake_minimum_required(VERSION 3.6)

set(CMAKE_CONFIGURATION_TYPES Debug Release CACHE TYPE INTERNAL FORCE)
set(LIBRARY_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../libs CACHE PATH "Path to lib folder")
project(project)
set(CMAKE_CXX_STANDARD 14)
set(Eigen3_INCLUDE_DIR ${LIBRARY_DIR}/Eigen/ CACHE PATH "Path to Eigen source folder")
find_package(Ceres REQUIRED)
find_package(glog REQUIRED)
add_definitions("-D_DISABLE_EXTENDED_ALIGNED_STORAGE")

# Define header and source files
set(UTILS
    utils/io.h
    utils/points.h
    utils/Eigen.h
    utils/json.hpp
    utils/smpl_model.hpp
    utils/smpl_model.cpp
)

add_executable(main ${UTILS} main.cpp)

target_link_libraries(main ceres)
target_include_directories(main PUBLIC ${Eigen3_INCLUDE_DIR})

