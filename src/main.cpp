#include <filesystem>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>

#include "utils/io.h"
#include "utils/points.h"
#include "utils/geometry.h"  //https://www.scratchapixel.com/code.php?id=24&origin=/lessons/3d-basic-rendering/3d-viewing-pinhole-camera&src=0
#include "utils/json.hpp"
#include "utils/smpl_model.hpp"
#include "utils/Eigen.h"

#include "ceres/ceres.h"
#include <math.h>
#include <array>

using json = nlohmann::json;


/////////////////////////// Helper classes and functions ///////////////////////

// A helper class for perspective camera projection
class Camera {
  public:
	Camera(float img_width, float img_height):
		m_img_width(img_width),
		m_img_height(img_height),
		m_focal_length(5000.0f)
	{}

	template<typename T>
	Eigen::Matrix<T, 3, 4> get_projection_matrix(
		const v3<T> &camera_translation) const
	{
		// Extrinsic transformation matrix
		// Rotation is defined in the first axis-angle vector of the model pose
		Eigen::Matrix<T, 4, 4> transform_extr;
		transform_extr.template block<3, 3>(0, 0).setIdentity();
		transform_extr.template block<3, 1>(0, 3) = -camera_translation;
		// Mirror the Z coordinate since the Z direction in SMPL points toward
		// the camera and not away from it
		transform_extr(2, 2) = T(-1);
		transform_extr(2, 3) = -transform_extr(2, 3);
		transform_extr.row(3) << T(0), T(0), T(0), T(1);
		// Intrinsic transformation matrix
		Eigen::Matrix<T, 3, 4> transform_intr;
		T f(m_focal_length);
		//~ T scale(m_img_width);
		T scale(1.0f);
		T cx(m_img_width * 0.5f);
		T cy(m_img_height * 0.5f);
		transform_intr.row(0) << f * scale, T(0), cx, T(0);
		// The y coordinate needs to be mirrored
		transform_intr.row(1) << T(0), -f * scale, cy, T(0);
		transform_intr.row(2) << T(0), T(0), T(1), T(0);
		return transform_intr * transform_extr;
	}

	template<typename T>
    std::vector<v2<T>> project_points(const v3<T> &translation,
		std::vector<v3<T>> points) const
	{
		// Calculate the projection matrix
		Eigen::Matrix<T, 3, 4> projection_matrix =
			get_projection_matrix(translation);
		// Collect all points with homgeneous coordinate in a single matrix
		int num_points = points.size();
		mX<T> points_homo(4, num_points);
		for (int i = 0; i < points.size(); i++) {
			points_homo.template block<3, 1>(0, i) = points[i];
			points_homo(3, i) = T(1);
		}
		// Do the projection
		mX<T> points_homo_projected = projection_matrix * points_homo;
		std::vector<v2<T>> projected_points(num_points);
		for (int i = 0; i < num_points; i++) {
			v3<T> point_2d_homo = points_homo_projected.col(i);
			projected_points[i] =
				point_2d_homo.template block<2, 1>(0, 0) / point_2d_homo(2);
		}
		return projected_points;
    }

public:
	float m_focal_length;
	float m_img_width;
	float m_img_height;
};


// A class which stores the OpenPose 2D joints and confidence values
class PoseOpenpose {
public:
	PoseOpenpose(const std::vector<Eigen::Vector2f> &joints,
			std::vector<float> confidentialities):
		m_joints(joints),
		m_confs(std::move(confidentialities))
	{}
	// Imports pose data from a json file
	PoseOpenpose(const std::string &file_path);

	const std::vector<Eigen::Vector2f> get_keypoints() const
		{ return m_joints; }

	const std::vector<float> get_confidentialities() const { return m_confs; }

public:
	std::vector<Eigen::Vector2f> m_joints;
	std::vector<float> m_confs;
};

// The json format is described at
// https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/02_output.md#json-output-format
PoseOpenpose::PoseOpenpose(const std::string &file_path)
{
	std::ifstream input_file(file_path);
	json j;
	input_file >> j;
	// Assume that there is exactly one detected person
	json pose_keypoints_2d = j["people"][0]["pose_keypoints_2d"];
	if (pose_keypoints_2d.size() != 75) {
		std::cout << "OpenPose json data does not have 75 keypoints: " <<
			j << std::endl;
	}
	for (int k = 0; k < pose_keypoints_2d.size() / 3; ++k) {
		Eigen::Vector2f pos;
		pos.x() = pose_keypoints_2d[k * 3];
		pos.y() = pose_keypoints_2d[k * 3 + 1];
		float confidentiality = pose_keypoints_2d[k * 3 + 2];
		m_joints.push_back(pos);
		m_confs.push_back(confidentiality);
	}
#if VERBOSE
	std::cout << "imported " << m_joints.size() <<
		" OpenPose keypoints from " << file_path << std::endl;
#endif
}


// Contains all the data for which we want to optimise for (for a single frame)
struct OutputData {
	OutputData():
		// Autodifferentiation may fail with zero pose values because of the
		// case distinction in rodriguez, so initialize it to 360° rotation
		//~ pose(24, Eigen::Vector3f(3.1416f * 2.0f, 0, 0)),
		pose(24, Eigen::Vector3f(0.01f, 0, 0)),
		shape(10, 0.0f),  // Ignore the shape for now
		camera_translation(Eigen::Vector3f(0, 0, 5.0f))
	{}
	std::vector<Eigen::Vector3f> pose;
	std::vector<float> shape;
	Eigen::Vector3f camera_translation;
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


void save_model_params(const std::string &json_path, OutputData &params)
{
	assert(params.pose.size() == 24);
	assert(params.shape.size() == 10);
	std::cout << "Saving the result to " << json_path << std::endl;
	std::ofstream output_file(json_path);
	json j;
	j["camera_translation"] = {params.camera_translation(0),
		params.camera_translation(1), params.camera_translation(2)};
	// SMPL eats pose values as flat array, so save them this way
	std::array<float, 24 * 3> pose_arr;
	for (int k = 0; k < 24; ++k) {
		pose_arr[3 * k + 0] = params.pose[k](0);
		pose_arr[3 * k + 1] = params.pose[k](1);
		pose_arr[3 * k + 2] = params.pose[k](2);
	}
	j["pose"] = pose_arr;
	j["shape"] = params.shape;
	output_file << j.dump();
}


/////////////////////////// Code related related to the optimisation ///////////


Eigen::Vector3f guess_init(const SMPLModel &model, const Camera &cam,
	const PoseOpenpose &op)
{
	// param j2d: 26x2 array of OpenPose joints
	// param init_pose: 72D vector of pose parameters used for initialization (kept fixed)
	// returns: Vector3f corresponding to the estimated camera translation

	//~ int smpl_ids[] = {-1, 14, 17, 19, 21, 16, 18, 20, -1, 2, 5, 8, 1, 4, 7, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};

	std::vector<Eigen::Vector3f> pose(24, Eigen::Vector3f(0,0,0));
	std::vector<float> shape(10, 0.0f);

	const MatrixXf &joints_rest = model.get_joints_rest();

	std::vector<Eigen::Vector3f> Jtr =
		model.calculate_joint_positions<float>(pose, shape);

    // 16 is L shoulder, 1 is L hip
    // 17 is R shoulder, 2 is R hip
	float sum3d = (Jtr[16] - Jtr[1]).norm() + (Jtr[17] - Jtr[2]).norm();
	float mean_h3d = sum3d / 2;

	std::vector<Eigen::Vector2f> j2d = op.get_keypoints();

    // 5 is L shoulder, 12 is L hip
    // 2 is R shoulder, 9 is R hip
	float sum2d = ((j2d[5] - j2d[12])).norm() + (j2d[2] - j2d[9]).norm();
	float mean_h2d = sum2d;// 2;

	std::cout << "Sum3d:" << sum3d << "Mean2d:" << sum2d<< "\n";
	std::cout << "Mean3d:" << mean_h3d << "Mean2d:" << mean_h2d<< "\n";

	float est_d = cam.m_focal_length * (mean_h3d / mean_h2d);
	return Eigen::Vector3f(0, 0, est_d);
}


// 	MODEL_25			SMPL (idx Breadth-first with Pelvis 0 [same as SMPLify])
//     {0,  "Nose"},			-
//     {1,  "Neck"},			Neck (14?)
//     {2,  "RShoulder"},		R_Shoulder (17)
//     {3,  "RElbow"},			R_Elbow (19)
//     {4,  "RWrist"},			R_Wrist (21)
//     {5,  "LShoulder"},		L_Shoulder (16)
//     {6,  "LElbow"},			L_Elbow (18)
//     {7,  "LWrist"},			L_Wrist (20)
//     {8,  "MidHip"},			-
//     {9,  "RHip"},			R_Hip (2)
//     {10, "RKnee"},			R_Knee (5)
//     {11, "RAnkle"},			R_Ankle (8)
//     {12, "LHip"},			L_Hip (1)
//     {13, "LKnee"},			L_Knee (4)
//     {14, "LAnkle"},			L_Ankle (7)
//     {15, "REye"},			-
//     {16, "LEye"},			-
//     {17, "REar"},			-
//     {18, "LEar"},			-
//     {19, "LBigToe"},			-
//     {20, "LSmallToe"},		-
//     {21, "LHeel"},			-
//     {22, "RBigToe"},			-
//     {23, "RSmallToe"},		-
//     {24, "RHeel"},			-
//     {25, "Background"}		-

// Additional SMPL joints: L,R_Foot[avg of 19,20], Spine1,2,3, L,R_Collar, L,R_Hand, Head [top is calculated in SMPLFY]

class CorrespondenceCostFunction {
public:
	CorrespondenceCostFunction(const SMPLModel &smpl_model, const Camera &cam,
			const PoseOpenpose &pose_openpose):
		m_smpl_model(smpl_model),
		m_camera(cam),
		m_pose_openpose(pose_openpose)
	{}

	template<typename T>
	bool operator()(const T* const pose_arr, const T* const translation_arr,
		T* residual) const
	{
		// Assemble the parameters
		std::vector<v3<T>> pose(24);
		for (int k = 0; k < 24; ++k) {
			pose[k] = v3<T>(pose_arr[k * 3 + 0], pose_arr[k * 3 + 1],
				pose_arr[k * 3 + 2]);
		}
		// Ignore the shape for now
		std::vector<T> shape(10, T(0));

		v3<T> cam_translation(translation_arr[0], translation_arr[1],
			translation_arr[2]);


		calculate_residual(pose, shape, cam_translation, residual);
		/*
		T residual_full[25];
		calculate_residual(pose, shape, cam_translation, residual_full);

		// keypoint errors
		std::array<int, 12> ids{11,10,9,12,13,14,4,3,2,5,6,7};
		for (int k = 0; k < ids.size(); ++k) {
			residual[k] = residual_full[ids[k]];
		}//*/

		return true;
	}

	// The residual is 25-dimensional (25 keypoints) although we set some
	// elements always to zero (where we have no correspondence)
	// The pose parameter contains 24 axis-angle vectors, where the first one
	// is the global rotation
	// The camera translation parameter contains a single vector
	static ceres::AutoDiffCostFunction<CorrespondenceCostFunction, 25,
		24 * 3, 3>
		*getCostFunction(const SMPLModel &smpl_model, const Camera &cam,
		const PoseOpenpose &pose_openpose)
	{
		return new ceres::AutoDiffCostFunction<CorrespondenceCostFunction, 25,
			24 * 3, 3>(new CorrespondenceCostFunction(smpl_model, cam,
			pose_openpose));
	}

	// Maps SMPL joints to corresponding 3D points for OpenPose (before
	// projection)
	// It also returns weights which indicate how well the point matches
	// Weight 0 means there's no correspondence and the point should be ignored
	template<typename T>
	static void SMPL_to_OpenPose(const std::vector<v3<T>> &jtr,
		std::vector<v3<T>> &points, std::vector<float> &weights)
	{
		// A map from OpenPose to SMPL
		const std::unordered_map<int, int> mapping{{ 0,15 }, { 1,12 },{ 2,17 },
			{ 3,19 },{ 4,21 },{ 5,16 },{ 6,18 },{ 7,20 },{ 9,2 },{ 10,5 },
			{ 11,8 },{ 12,1 },{ 13,4 },{ 14,7 }, {19, 10}, {22, 11} };
		// OpenPose has 25 keypoints
		points.resize(25);
		weights.resize(25, 0.0f);
		for (const auto& it : mapping) {
			int id_openpose = it.first;
			int id_smpl = it.second;
			weights[id_openpose] = 1.0f;
			points[id_openpose] = jtr[id_smpl];
		}
		// TODO: Add more 3D keypoint positions by calculating weighted sums
		// of SMPL joint positions
	}

protected:
	// The main residual calculation.
	// The residual should be an array of size 25.
	template<typename T>
	void calculate_residual(std::vector<v3<T>> pose, std::vector<T> shape,
		const v3<T> &cam_translation, T *residual) const
	{
		// Calculate SMPL joint locations for the given pose and shape
		std::vector<v3<T>> jtr =
			m_smpl_model.calculate_joint_positions<T>(pose, shape);

		// Calculate 2D OpenPose keypoints for these SMPL joint locations
		std::vector<v3<T>> points_3d_openpose;
		std::vector<float> points_weights;
		SMPL_to_OpenPose<T>(jtr, points_3d_openpose, points_weights);
		std::vector<v2<T>> points_2d =
			m_camera.project_points<T>(cam_translation, points_3d_openpose);

		//~ std::cout << "point5: "<< points_2d[5].transpose() << std::endl;

		// Calculate the error
		std::vector<Eigen::Vector2f> reference_keypoints =
			m_pose_openpose.get_keypoints();
		std::vector<float> confidentialities =
			m_pose_openpose.get_confidentialities();
		for (int k = 0; k < 25; ++k) {
			float w = points_weights[k];
			if (w == 0) {
				// There's no point correspondence
				residual[k] = T(0);
				continue;
			}
			v2<T> dif = points_2d[k] - v2<T>(reference_keypoints[k](0),
				reference_keypoints[k](1));
			T distance = dif.norm();
			residual[k] = T(w * confidentialities[k]) * distance;
		}
	}


	const SMPLModel &m_smpl_model;
	const Camera &m_camera;
	const PoseOpenpose &m_pose_openpose;
};


class InitCamCostFunction : public CorrespondenceCostFunction {
public:
	InitCamCostFunction(const SMPLModel &smpl_model, const Camera &cam,
			const PoseOpenpose &pose_openpose, const Eigen::Vector3f &init_t):
		CorrespondenceCostFunction(smpl_model, cam, pose_openpose),
		m_init_t(init_t)
	{}

	template<typename T>
	bool operator()(const T* const translation_arr,
		const T* const model_rotation, T* residual) const
	{
		// Assemble the parameters
		//~ std::vector<v3<T>> pose(24, v3<T>(T(3.1416 * 2.0), T(0), T(0)));
		std::vector<v3<T>> pose(24, v3<T>(T(0.01f), T(0), T(0)));
		std::vector<T> shape(10, T(0));
		pose[0] = v3<T>(model_rotation[0], model_rotation[1],
			model_rotation[2]);
		v3<T> cam_translation(translation_arr[0], translation_arr[1],
			translation_arr[2]);

		T residual_full[25];
		calculate_residual(pose, shape, cam_translation, residual_full);

		// Torso keypoint errors
		// right hip, left hip, right shoulder, left shoulder
		std::array<int, 4> torso_ids{9, 12, 2, 5};
		for (int k = 0; k < torso_ids.size(); ++k) {
			residual[k] = residual_full[torso_ids[k]];
		}
		// Regularizer
		residual[4] = T(100.0f) * (cam_translation[2] - T(m_init_t(2)));

		return true;
	}

	// The residual is 5-dimensional (one regularizer and 4 torso errors)
	// The camera translation parameter contains a single vector
	// The axis angle model rotation parameter also contains a single vector
	static ceres::AutoDiffCostFunction<InitCamCostFunction, 5, 3, 3>
		*getCostFunction(const SMPLModel &smpl_model, const Camera &cam,
		const PoseOpenpose &pose_openpose, const Eigen::Vector3f &init_t)
	{
		return new ceres::AutoDiffCostFunction<InitCamCostFunction, 5,
			3, 3>(new InitCamCostFunction(smpl_model, cam,
			pose_openpose, init_t));
	}

private:
	// Initial estimated camera translation
	Eigen::Vector3f m_init_t;

public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};


class UnrealisticPoseCostFunction {
public:
	UnrealisticPoseCostFunction(const SMPLModel &smpl_model):
		m_smpl_model(smpl_model)
	{}

	template<typename T>
	bool operator()(const T* const pose_arr, T* residual) const
	{
		// Assemble the parameters
		std::vector<v3<T>> pose(24);
		for (int k = 0; k < 24; ++k) {
			pose[k] = v3<T>(pose_arr[k * 3 + 0], pose_arr[k * 3 + 1],
				pose_arr[k * 3 + 2]);
		}
		// Calculate the error
		residual[0] = m_smpl_model.unrealistic_pose_error<T>(pose);
		return true;
	}

	static ceres::AutoDiffCostFunction<UnrealisticPoseCostFunction, 1, 24 * 3>
		*getCostFunction(const SMPLModel &smpl_model)
	{
		return new ceres::AutoDiffCostFunction<UnrealisticPoseCostFunction, 1,
			24 * 3>(new UnrealisticPoseCostFunction(smpl_model));
	}

private:
	const SMPLModel &m_smpl_model;
};


class NondefaultPoseCostFunction {
public:
	NondefaultPoseCostFunction(const SMPLModel &smpl_model):
		m_smpl_model(smpl_model)
	{}

	template<typename T>
	bool operator()(const T* const pose_arr, T* residual) const
	{
		// Assemble the parameters
		std::vector<v3<T>> pose(24);
		for (int k = 0; k < 24; ++k) {
			pose[k] = v3<T>(pose_arr[k * 3 + 0], pose_arr[k * 3 + 1],
				pose_arr[k * 3 + 2]);
		}
		// Calculate the error.
		// It is weighted very low weight since this cost function
		// is only used to avoid underdeterminism.
		m_smpl_model.nondefault_pose_error<T>(pose, residual);
		return true;
	}

	static ceres::AutoDiffCostFunction<NondefaultPoseCostFunction, 24, 24 * 3>
		*getCostFunction(const SMPLModel &smpl_model)
	{
		return new ceres::AutoDiffCostFunction<NondefaultPoseCostFunction, 24,
			24 * 3>(new NondefaultPoseCostFunction(smpl_model));
	}

private:
	const SMPLModel &m_smpl_model;
};


void optimize_init(const SMPLModel &model, const Camera &cam,
	const PoseOpenpose &op, OutputData &result)
{
	Eigen::Vector3f cam_translation_init = guess_init(model, cam, op);
	//~ cam_translation_init(2) = 5.690184013540835;
	std::cout << "initial translation: " <<
		cam_translation_init.transpose() << std::endl;

	double cam_translation[3];
	for (int i = 0; i < 3; ++i) {
		cam_translation[i] = cam_translation_init(i);
	}
	//~ double model_rotation[3] = {3.1416 * 2.0, 0.0, 0.0};
	double model_rotation[3] = {0.01, 0.0, 0.0};

	std::cout << "Initial optimization" << std::endl;

	ceres::Problem problem;

	ceres::CostFunction* cost_function = InitCamCostFunction::getCostFunction(
		model, cam, op, cam_translation_init);
	problem.AddResidualBlock(cost_function, nullptr, cam_translation,
		model_rotation);

	ceres::Solver::Options options;
	options.max_num_iterations = 140;
	options.linear_solver_type = ceres::DENSE_QR;
	//~ options.trust_region_strategy_type = ceres::DOGLEG;
	//~ options.dogleg_type = ceres::SUBSPACE_DOGLEG;
	options.minimizer_progress_to_stdout = true;
	ceres::Solver::Summary summary;
	ceres::Solve(options, &problem, &summary);
	std::cout << summary.BriefReport() << std::endl;

	for (int i = 0; i < 3; ++i) {
		result.camera_translation(i) = static_cast<float>(cam_translation[i]);
		result.pose[0](i) = static_cast<float>(model_rotation[i]);
	}
	//~ result.camera_translation = Eigen::Vector3f(0.02010573, 0.39144794, 5.62350063);
	//~ result.pose[0] = Eigen::Vector3f(-4.65868156e+01, -4.82171990e-03, -7.43371840e+00);
	std::cout << "Found approximate translation: " <<
		result.camera_translation.transpose() << std::endl;
}

void optimize(SMPLModel &model, Camera &cam, PoseOpenpose &op,
	OutputData &result)
{
	// Initialize the parameters for the optimization
	double pose_params[24 * 3];
	for (int k = 0; k < 24; ++k) {
		pose_params[3 * k + 0] = result.pose[k](0);
		pose_params[3 * k + 1] = result.pose[k](1);
		pose_params[3 * k + 2] = result.pose[k](2);
	}

	double cam_translation[3];
	for (int i = 0; i < 3; ++i) {
		cam_translation[i] = result.camera_translation(i);
	}

	ceres::Problem problem;

	ceres::CostFunction* cost_function =
		CorrespondenceCostFunction::getCostFunction(model, cam, op);
	problem.AddResidualBlock(cost_function, nullptr, pose_params,
		cam_translation);

	ceres::CostFunction* unrealistic_pose_cost_function =
		UnrealisticPoseCostFunction::getCostFunction(model);
	problem.AddResidualBlock(unrealistic_pose_cost_function, nullptr,
		pose_params);

	ceres::CostFunction* nondefault_pose_cost_function =
		NondefaultPoseCostFunction::getCostFunction(model);
	problem.AddResidualBlock(nondefault_pose_cost_function, nullptr,
		pose_params);

	ceres::Solver::Options options;
	options.max_num_iterations = 140;
	options.linear_solver_type = ceres::DENSE_QR;
	//~ options.trust_region_strategy_type = ceres::DOGLEG;
	//~ options.dogleg_type = ceres::SUBSPACE_DOGLEG;
	options.minimizer_progress_to_stdout = true;
	ceres::Solver::Summary summary;
	ceres::Solve(options, &problem, &summary);
	std::cout << summary.BriefReport() << std::endl;

	// Return the found SMPL parameters and camera translation
	result.shape.resize(10, 0.0f);  // Ignore the shape for now
	result.pose.resize(24);
	for (int k = 0; k < 24; ++k) {
		result.pose[k] = Eigen::Vector3f(
			static_cast<float>(pose_params[3 * k + 0]),
			static_cast<float>(pose_params[3 * k + 1]),
			static_cast<float>(pose_params[3 * k + 2]));
	}
	for (int i = 0; i < 3; ++i) {
		result.camera_translation(i) = static_cast<float>(cam_translation[i]);
	}
}


//////////////////////////////// Example and testing code //////////////////////


void test_joint_calculation(const SMPLModel &model)
{
	std::cout << "testing joint calculation outputs" << std::endl;
	std::vector<Eigen::Vector3f> pose(24, Eigen::Vector3f(0,0,0));
	std::vector<float> shape(10, 0.0f);

	const MatrixXf &joints_rest = model.get_joints_rest();

	std::vector<Eigen::Vector3f> jtr =
		model.calculate_joint_positions<float>(pose, shape);
	// calculate_joint_positions must return the rest pose joints if the pose
	// values are all zero
	for (int k = 0; k < 24; ++k) {
		Eigen::Vector3f joint_rest = joints_rest.row(k).transpose();
		assert((jtr[k] - joint_rest).norm() < 0.01f);
	}
	// Compare some rest pose joint locations with known values
	std::unordered_map<int, Eigen::Vector3f> rest_joints_reference = {
		{17, Eigen::Vector3f(-0.191692f, 0.236929f, -0.0123055f)},
		{19, Eigen::Vector3f(-0.45182f, 0.222559f, -0.0435742f)},
		{21, Eigen::Vector3f(-0.720928f, 0.229353f, -0.049601f)},
		{2, Eigen::Vector3f(-0.0624834f, -0.331302f, 0.0150413f)},
		{8, Eigen::Vector3f(-0.0866845f, -1.13504f, -0.0243634f)},
	};
	for (const auto &el : rest_joints_reference) {
		Eigen::Vector3f joint_ref = el.second;
		Eigen::Vector3f joint_current = joints_rest.row(el.first);
		assert((joint_ref - joint_current).norm() < 0.01f);
	}
	// Rotate the right arm counter-clockwise around Z by 90°
	pose[17] = Eigen::Vector3f(0, 0, 3.141 / 2);
	// Rotate the left leg counter-clockwise arount X
	pose[5] = Eigen::Vector3f(3.141 / 2, 0, 0);
	jtr = model.calculate_joint_positions<float>(pose, shape);
	// Compare some transformed joint locations with known values
	std::unordered_map<int, Eigen::Vector3f> moved_joints_reference = {
		{17, rest_joints_reference[17]},
		{19, Eigen::Vector3f(-0.1774f, -0.023203f, -0.0435742f)},
		{21, Eigen::Vector3f(-0.184274f, -0.292309f, -0.049601f)},
		{2, rest_joints_reference[2]},
		{8, Eigen::Vector3f(-0.0866845f, -0.680553f, -0.409858f)},
	};
	for (const auto &el : moved_joints_reference) {
		Eigen::Vector3f joint_ref = el.second;
		Eigen::Vector3f joint_current = jtr[el.first];
		assert((joint_ref - joint_current).norm() < 0.01f);
	}
}

void test_projection(const SMPLModel &model)
{
	std::cout << "testing the camera projection" << std::endl;
	float img_width = 200;
	float img_height = 100;
	Camera cam(img_width, img_height);
	Vector3f camera_translation(0, 0, 5.0f);


	// Test if the projecting fulfills well known properties

	std::vector<Eigen::Vector3f> points_3d(6);
	points_3d[0] = Eigen::Vector3f(0, 0, 0);
	points_3d[1] = Eigen::Vector3f(0, 0, 0.7f);
	points_3d[2] = Eigen::Vector3f(0.4, 0.5, 0);
	points_3d[3] = Eigen::Vector3f(0.4, 0.5, 0.7f);
	points_3d[4] = Eigen::Vector3f(-0.4, -0.5, 0);
	points_3d[5] = Eigen::Vector3f(-0.4, -0.5, 0.7f);
	std::vector<Eigen::Vector2f> points_2d = cam.project_points<float>(
		camera_translation, points_3d);
	Eigen::Vector2f image_centre(img_width / 2, img_height / 2);
	/*
	std::cout << "image centre: " << image_centre.transpose() << std::endl;
	for (int j = 0; j < 6; ++j) {
		std::cout << points_3d[j].transpose() << " -> " <<
			points_2d[j].transpose() << std::endl;
	}
	//*/
	// Positions with zero X and Y values should be projected to the image
	// centre
	assert((points_2d[0] - image_centre).norm() < 0.001f);
	assert((points_2d[1] - image_centre).norm() < 0.001f);
	// Positive deviation in X and Y means upper right to the image centre
	assert(points_2d[2](0) > image_centre(0));
	assert(points_2d[2](1) < image_centre(1));
	assert(points_2d[3](0) > image_centre(0));
	assert(points_2d[3](1) < image_centre(1));
	// Negative deviation in X and Y means lower left to the image centre
	assert(points_2d[4](0) < image_centre(0));
	assert(points_2d[4](1) > image_centre(1));
	assert(points_2d[5](0) < image_centre(0));
	assert(points_2d[5](1) > image_centre(1));
	// Higher value of Z (closer to the camera) means a larger distance
	// to the image centre
	assert(points_2d[2](0) < points_2d[3](0));
	assert(points_2d[2](1) > points_2d[3](1));
	assert(points_2d[4](0) > points_2d[5](0));
	assert(points_2d[4](1) < points_2d[5](1));


	// Test if projecting the rest pose works

	const MatrixXf &joints_rest_mat = model.get_joints_rest();
	std::vector<Eigen::Vector3f> joints_rest(24);
	for (int k = 0; k < 24; ++k) {
		joints_rest[k] = joints_rest_mat.row(k).transpose();
	}
	// SMPL coordinates go from bottom left to top right
	assert(joints_rest[19](0) < joints_rest[1](0));
	assert(joints_rest[19](1) > joints_rest[1](1));
	std::vector<Eigen::Vector2f> projected_points = cam.project_points(
		camera_translation, joints_rest);
	assert(projected_points.size() == 24);
	// Image coordinates go from top left to bottom right
	assert(projected_points[19](0) < projected_points[1](0));
	assert(projected_points[19](1) < projected_points[1](1));
}


void test_initial_optimisation(const SMPLModel &model)
{
	std::cout << "testing the initial optimisation" << std::endl;
	// Calculate OpenPose 2D keypoints from SMPL parameters and pretend that
	// they are generated by OpenPose

	std::vector<Eigen::Vector3f> pose(24, Eigen::Vector3f(0.01f, 0, 0));
	std::vector<float> shape(10, 0.0f);
	Eigen::Vector3f cam_translation(1.0f, 2.0f, 15.0f);

	// Calculate SMPL joint locations for the given pose and shape
	std::vector<Eigen::Vector3f> jtr =
		model.calculate_joint_positions<float>(pose, shape);

	// Calculate 2D OpenPose keypoints for these SMPL joint locations
	std::vector<Eigen::Vector3f> points_3d_openpose;
	std::vector<float> points_weights;
	CorrespondenceCostFunction::SMPL_to_OpenPose<float>(jtr, points_3d_openpose,
		points_weights);
	assert(points_weights.size() == 25);
	float img_width = 200;
	float img_height = 100;
	Camera cam(img_width, img_height);
	std::vector<Eigen::Vector2f> points_2d =
		cam.project_points<float>(cam_translation, points_3d_openpose);
	assert(points_2d.size() == 25);
	/*
	std::cout << "points_2d is:" << std::endl;
	for (int k = 0; k < 25; ++k) {
		std::cout << points_2d[k].transpose() << std::endl;
	}//*/
	PoseOpenpose op(points_2d, points_weights);

	// Find cam_translation for this forged PoseOpenpose.
	// This optimization should work (but doesn't work accurately now)

	OutputData result;
	optimize_init(model, cam, op, result);

	std::cout << "translation: " << cam_translation.transpose() << std::endl <<
		"calculated trans: " << result.camera_translation.transpose() <<
		std::endl;
	assert((result.camera_translation - cam_translation).norm() < 0.01f);

}

void run_tests(const SMPLModel &smpl_model)
{
	test_joint_calculation(smpl_model);
	test_projection(smpl_model);
	//test_initial_optimisation(smpl_model);
}


////////////////////////////////////////////////////////////////////////////////


int main(int argc, char** argv)
{
	//Hardcoded aguments for now:
	std::string smpl_model_path = "../models/smpl_male.json";
	std::string openpose_example_path =
		"../test_images/mpv-shot0001_keypoints.json";
	// The image size refers to the detected OpenPose keypoint coordinates
	float image_width = 1920;
	float image_height = 1080;
	std::string out_dir = "../results";

	// TODO: Check and create dirs, use optparse for arguments

	// Initialisation
	SMPLModel model(smpl_model_path);
	Camera cam(image_width, image_height);

	// Find good initial parameters
	PoseOpenpose op(openpose_example_path);
	OutputData result;


	optimize_init(model, cam, op, result);


	// Optimize for a single image
	optimize(model, cam, op, result);
	save_model_params(out_dir + "/1.json", result);
//*/

	run_tests(model);

	return 0;
}

