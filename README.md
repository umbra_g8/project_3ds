# Real-Time RGB-Based Body Animation

Setup:

* Copy the SMPL models folder from the SMPL Python2 implementation to the root folder
* Convert the SMPL models to json:
	`python2 ./scripts/preprocess.py male <path to the male .pkl model> ./models/`
	The command is analogous for the female model.
* Install the Ceres and Eigen libraries
