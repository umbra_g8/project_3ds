#!/usr/bin/env python2
# This script works when put inside smpl/smpl_webuser/hello_world/

import numpy as np
import argparse
import json

from smpl_webuser.serialization import load_model


def parse_arguments():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("json_path", help="JSON file with SMPL parameters")
    return parser.parse_args()

def main():
	args = parse_arguments()
	model = load_model( '../../models/basicModel_f_lbs_10_207_0_v1.0.0.pkl' )

	with open(args.json_path, "r") as json_file:
		params = json.load(json_file)
	model.pose[:] = np.array(params["pose"])
	model.betas[:] = np.zeros(model.betas.size)

	output_path = "./tmp_result.obj"
	with open(output_path, "w") as outfile:
		for v in model.r:
			outfile.write("v {:f} {:f} {:f}\n".format(v[0], v[1], v[2]))

		for f in model.f+1:
			outfile.write("f {:d} {:d} {:d}\n".format(f[0], f[1], f[2]))
	print("Resulting mesh saved to {}".format(output_path))

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
